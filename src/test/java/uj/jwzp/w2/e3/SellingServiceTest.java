package uj.jwzp.w2.e3;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import uj.jwzp.w2.e3.external.PersistenceLayer;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class SellingServiceTest {

    private final BigDecimal ITEM_PRICE = BigDecimal.valueOf(3);
    private final BigDecimal DISCOUNT = BigDecimal.valueOf(10);
    private final String ITEM_NAME = "item name";
    private final Item i = new Item(ITEM_NAME, ITEM_PRICE);
    private final Customer c = new Customer(1, "DasCustomer", "Kraków, Łojasiewicza");
    private final int ITEM_QUANTITY = 7;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private PersistenceLayer persistenceLayer;
    @Mock
    private WeekendPromotion weekendPromotion;
    @Mock
    private CustomerMoneyService customerMoneyService;

//    @Before
//    public void init() {
//
//    }

    @Test
    public void should_save_transaction() {
        //given
        SellingService uut = new SellingService(persistenceLayer);
        uut.moneyService = customerMoneyService;
        when(customerMoneyService.pay(any(), any())).thenReturn(true);
        when(persistenceLayer.saveCustomer(Mockito.any())).thenReturn(true);
        when(persistenceLayer.saveTransaction(any(), any(), anyInt())).thenReturn(true);
        when(weekendPromotion.checkWeekendPromotion()).thenReturn(false);
        when(weekendPromotion.getDiscountForItem(any(), any())).thenReturn(DISCOUNT);

        //when
        boolean sold = uut.sell(weekendPromotion, i, ITEM_QUANTITY, c);

        //then
        verify(persistenceLayer, times(1)).saveTransaction(any(), any(), anyInt());
    }

    @Test
    public void should_not_save_transaction() {
        //given
        SellingService uut = new SellingService(persistenceLayer);
        uut.moneyService = customerMoneyService;
        when(customerMoneyService.pay(any(), any())).thenReturn(false);
        when(persistenceLayer.saveCustomer(Mockito.any())).thenReturn(true);
        when(persistenceLayer.saveTransaction(any(), any(), anyInt())).thenReturn(true);
        when(weekendPromotion.checkWeekendPromotion()).thenReturn(false);
        when(weekendPromotion.getDiscountForItem(any(), any())).thenReturn(DISCOUNT);

        //when
        boolean sold = uut.sell(weekendPromotion, i, ITEM_QUANTITY, c);

        //then
        verify(persistenceLayer, never()).saveTransaction(any(), any(), anyInt());
    }
//
//    @Test
//    public void sellWithoutPromotion() {
//        //given
//        SellingService uut = new SellingService(persistenceLayer);
//        Mockito.when(persistenceLayer.saveCustomer(Mockito.any())).thenReturn(Boolean.TRUE);
//        Item i = new Item("i", new BigDecimal(3));
//        Customer c = new Customer(1, "DasCustomer", "Kraków, Łojasiewicza");
//        WeekendPromotion weekendPromotion = mock(WeekendPromotion.class);
//        when(weekendPromotion.checkWeekendPromotion()).thenReturn(false);
//
//        //when
//        boolean sold = uut.sell(weekendPromotion, i, 1, c);
//
//        //then
//        Assert.assertFalse(sold);
//        Assert.assertEquals(BigDecimal.valueOf(7), uut.moneyService.getMoney(c));
//    }
//
//    @Test
////	@PrepareForTest(DiscountsConfig.class)
//    public void sellALotWithPromotion() {
//        //given
//        SellingService uut = new SellingService(persistenceLayer);
//        Mockito.when(persistenceLayer.saveCustomer(Mockito.any())).thenReturn(Boolean.TRUE);
//        Item i = new Item("i", new BigDecimal(3));
//        Customer c = new Customer(1, "DasCustomer", "Kraków, Łojasiewicza");
//        uut.moneyService.addMoney(c, new BigDecimal(990));
//        WeekendPromotion weekendPromotion = mock(WeekendPromotion.class);
//        when(weekendPromotion.checkWeekendPromotion()).thenReturn(true);
//
//        //when
//        boolean sold = uut.sell(weekendPromotion, i, 10, c);
//
//        //then
//        Assert.assertFalse(sold);
//        Assert.assertTrue(c.getName().equals("DasCustomer"));
//        Assert.assertTrue(i.getName().equals("i"));
//        Assert.assertEquals(BigDecimal.valueOf(973), uut.moneyService.getMoney(c));
//    }
//
//    @Test
//    public void sellALotWithoutPromotion() {
//        //given
//        SellingService uut = new SellingService(persistenceLayer);
//        Mockito.when(persistenceLayer.saveCustomer(Mockito.any())).thenReturn(Boolean.TRUE);
//        Item i = new Item("i", new BigDecimal(3));
//        Customer c = new Customer(1, "DasCustomer", "Kraków, Łojasiewicza");
//        uut.moneyService.addMoney(c, new BigDecimal(990));
//        WeekendPromotion weekendPromotion = mock(WeekendPromotion.class);
//        when(weekendPromotion.checkWeekendPromotion()).thenReturn(false);
//
//        //when
//        boolean sold = uut.sell(weekendPromotion, i, 10, c);
//
//        //then
//        Assert.assertEquals(BigDecimal.valueOf(970), uut.moneyService.getMoney(c));
//    }
}
