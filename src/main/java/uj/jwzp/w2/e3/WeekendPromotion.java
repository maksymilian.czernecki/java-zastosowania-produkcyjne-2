package uj.jwzp.w2.e3;

import uj.jwzp.w2.e3.external.DiscountsConfig;

import java.math.BigDecimal;

public class WeekendPromotion {
    public BigDecimal getDiscountForItem(Item item, Customer customer) {return DiscountsConfig.getDiscountForItem(item, customer);}

    boolean checkWeekendPromotion() {
        return DiscountsConfig.isWeekendPromotion();
    }

}
