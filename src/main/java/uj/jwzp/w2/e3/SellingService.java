package uj.jwzp.w2.e3;

import uj.jwzp.w2.e3.external.PersistenceLayer;

import java.math.BigDecimal;

public class SellingService {

    private final PersistenceLayer persistenceLayer;
    CustomerMoneyService moneyService;

    public SellingService(PersistenceLayer persistenceLayer) {
        this.persistenceLayer = persistenceLayer;
        this.persistenceLayer.loadDiscountConfiguration();
        moneyService = new CustomerMoneyService(this.persistenceLayer);
    }

    public boolean sell(WeekendPromotion weekendPromotion, Item item, int quantity, Customer customer) {
        BigDecimal price = item.getPrice().subtract(weekendPromotion.getDiscountForItem(item, customer)).multiply(BigDecimal.valueOf(quantity));
        if (weekendPromotion.checkWeekendPromotion() && price.compareTo(BigDecimal.valueOf(5)) > 0) {
            price = price.subtract(BigDecimal.valueOf(3));
        }
        boolean sold = moneyService.pay(customer, price);
        if (sold) {
            return persistenceLayer.saveTransaction(customer, item, quantity);
        } else {
            return sold;
        }
    }

}
