package uj.jwzp.w2.e3;

import uj.jwzp.w2.e3.external.PersistenceLayer;

import java.math.BigDecimal;
import java.util.HashMap;

public class CustomerMoneyService {

    private final PersistenceLayer persistenceLayer;
    HashMap<Customer, BigDecimal> customerMoney = new HashMap<>();

    public CustomerMoneyService(PersistenceLayer persistenceLayer) {
        this.persistenceLayer = persistenceLayer;
    }

    public BigDecimal getMoney(Customer customer) {

        if (customerMoney.containsKey(customer)) {
            return customerMoney.get(customer);
        } else {
            customerMoney.put(customer, BigDecimal.TEN);
            persistenceLayer.saveCustomer(customer);
            return customerMoney.get(customer);
        }
    }

    public boolean pay(Customer customer, BigDecimal amount) {
        BigDecimal money = getMoney(customer);
        if (money.compareTo(amount) >= 0) {
            customerMoney.put(customer, money.subtract(amount));
            persistenceLayer.saveCustomer(customer);
            return true;
        }
        return false;
    }

    public void addMoney(Customer customer, BigDecimal amount) {
        BigDecimal money = getMoney(customer);
        persistenceLayer.saveCustomer(customer);
        customerMoney.put(customer, money.add(amount));
    }

}
